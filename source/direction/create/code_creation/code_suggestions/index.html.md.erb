---
layout: sec_direction
title: "Category Direction - Code Suggestions"
description: "Code recommendations and suggestions within IDE editor extensions"
canonical_path: "/direction/create/code_creation/code_suggestions"
noindex: true
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Code Suggestions

| | |
| --- | --- |
| Stage | [Create](/direction/create/) |
| Group | [Code Creation](/direction/code_creation) |
| Maturity | [minimal](/direction/maturity/) |
| Content Last Reviewed | `2023-04-10` |

### Introduction and how you can help

Thanks for visiting this category direction page on Code suggestions in GitLab. This page belongs to the [AI Assisted](/..) group of the [ModelOps stage](../modelops) and is maintained by Neha Khalwadekar (nkhalwadekar at gitlab dot com).

This direction page is a work in progress, and [everyone can contribute](https://about.gitlab.com/company/mission/#contribute-to-gitlab-application). We welcome feedback, [bug reports](https://gitlab.com/gitlab-org/gitlab/-/issues/new?&issuable_template=Bug), [feature proposals](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Feature%20proposal%20-%20detailed), and [community contributions](https://about.gitlab.com/community/contribute/)..

 - Please comment and contribute in the linked [issues](#TODO) and [epics](#TODO) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email or on a video call. If you're a GitLab user and have direct knowledge of your need for code suggestions, we&apos;d especially love to hear from you.
- Consider signing up for [First Look](https://about.gitlab.com/community/gitlab-first-look/).

## Overview

We intend to make code suggestions available for any external IDE and terminal for ultimate users, starting with GitLab's VS Code plugin and new Web IDE. GitLab aspires to build AI-based solutions to increase developer productivity by helping them code faster and more efficiently. Get code suggestions that match a repository context and style conventions, and cycle through different options to decide what to accept, reject, or edit.

![AI/ML Market Opportunity](/images/direction/modelops/AI-ML-Market-opportunity.png)

### Target Audience

Initially, we are focused on traditional developer personas including:
1. [Sasha (Software Developer)](https://about.gitlab.com/handbook/product/personas/#sasha-software-developer)
1. [Priyanka (Platform Engineer)](https://about.gitlab.com/handbook/product/personas/#priyanka-platform-engineer)
1. [Simone (Software Engineer in Test)](https://about.gitlab.com/handbook/product/personas/#simone-software-engineer-in-test)
1. [Delaney (Development Team Lead)](https://about.gitlab.com/handbook/product/personas/#delaney-development-team-lead)

In the future, we may expand to security personas to help write more secure code and review code for security vulnerabilities and fix them early in the software development lifecycle (SDLC), before you commit.

### Challenges to Address

By implementing AI Assisted Code Suggestions in integrated development environments (IDEs), we intend to transform the software development lifecycle for our customers in the following ways:

1. **Increased productivity**: Empower Developers to write code faster and more efficiently, with code suggestions for code snippets and improvements in real time.
1. **Reduced errors**: Detect and suggest corrections for errors as they occur, reducing the time and effort spent on debugging.
1. **Improved code quality**: Suggest refactoring and best practices, leading to more maintainable and performant code.
1. **More efficient collaboration**: Developers should be able to share and reuse code snippets with their team members, leading to more efficient collaboration.
1. **Access to a broader range of knowledge**: Suggest code snippets and improvements across a wide range of programming languages and platforms, providing developers access to a broader range of knowledge.

Overall, the integration of AI Assisted Code Suggestions should greatly improve the efficiency, quality, and speed of the software development process. We are working through several constraints such as legal, and data privacy concerns in order to make the best experience available for our customers. 

## Key Features

Smart code completion
1. **Support faster and more accurate code completion** without analyzing the context in the file you are editing or file type.
1. **Ability to pick from various suggestions including context** allowing for developers to direct recommendations towards a specific solution more easily. 
1. **Generate suggestions for APIs and frameworks** allowing developers to fill in low-value common boilerplate code.
1. **Ability to turn suggestions on|off as needed** extending greater control over the developer IDE experience.

Test case generation and automation
1. **Ability to generate Code Completions** for gitlab-ci.yml files.

Programming in natural language
1. **Generate code suggestions from plain text** allowing suggestions to take direction and have a greater context of developer intention.

#### What is our Vision (Long-term Roadmap)

In the long run, we intend to revolutionize the software development process through the integration of AI-assisted code suggestions, resulting in increased productivity, reduced errors, and improved overall software quality. Furthermore, we intend to enrich the experience by introducing NLP search in our WebIDE to help developers speed up the development process. 

#### What's Next & Why (Near-term Roadmap)

With the Beta release, we are now focused on providing functionalities like code completion, code recommendations as well as code fillers to improve the quality of the suggestions by Q2, 2023. With that, We plan on extending our language support to 13 programming languages. Currently, code suggestions are available to use in Visual Studio Code via GitLab workflow extensions. We will soon add support for the new GitLab WebIDE. We are actively adding monitoring and infrastructure support to better scale and optimize code suggestions. We will continue iterating on early beta user feedback and enhance the underlying models, the inference backend, and extend support for other IDEs.

We want code suggestions to feel natural and not get in the way, we will be improving the UX for code suggestions experience to ensure developers enjoy a seamless experience as they code. To improve the quality of the code suggestions, We are exploring merging models via request routing to have multiple specialized models handling the request types that are best suited to handle. 
we are also exploring code suggestions for GitLab-ci.yml files for MVC - An additional use case that can help us pave the way for future training on customer code and explore test harness generation to help developers automate the creation of tests by generating them through code suggestions.

Smart code completion to optimize code 
Summary: How might we ensure developers can write code more quickly and precisely, which completes code snippets based on context? This may lead to fewer mistakes and more effective development.
1. **Autocomplete**: Offers code completion suggestions based on what has already been written. It can suggest the next word, function, or parameter to speed up coding.
1. **Code Snippets**: Suggests prewritten code fragments that can be placed into the current code to save time and effort. The code editor may offer these snippets, or they may be user-defined.
1. **Predictive Analysis with real-time debugging**: To expedite development, examine the written code and suggest the following logical step or functions and provide real-time suggestions for enhancements and debugging suggestions.
1. **Variable naming**: Suggest variable names that are more relevant and descriptive so that your code is simpler to read and comprehend.
1. **Code commenting**: Consider adding comments to your code to clarify its purpose and make it simpler for others to read, comprehend, and maintain.
1. **Code documentation**: Automatically generate documentation to make it simpler for peers to use and comprehend your code.
1. **Code completion with NL**: Based on the natural language text context, suggest code recommendations to make coding simpler and faster.
1. **Code reuse**: Suggest repurposing previously created code to reduce duplication and increase code efficiency.
1. **Error handling**: Provide recommendations for how to deal with errors in your code to keep it stable and prevent crashes.


#### What is Not Planned Right Now

Currently, we are not developing a fully original GitLab-trained model, however, that is something we may consider in the future. For now, we will focus on open-source code generation models.

Continuous integration and deployment
Summary: How might we facilitate continuous integration and deployment by identifying code changes that could cause potential conflicts? This will enable developers to resolve issues quickly and roll out production code faster.
1. **Code Quality Analysis**: Examine the codebase to find problems like duplicate code, code smells, and other code quality problems. This aids programmers in enhancing code quality and lowering technical debt.
1. **Code Review Assistant**: Provide suggestions for review script changes to flagging out potential problems and give suggestions for enhancements. This can save time for developers by you can make it easier to find errors before they cause issues.
1. **Continuous Testing**: Automate testing procedures and determine when tests should be rerun based on code changes. This assures that the code is always rigorously tested and aids teams in identifying problems early in the development process.
1. **Code summarization**: This feature summarizes code by extracting the most important information from it. This helps the programmer to understand code more quickly and easily.
1. **Code Style Suggestions**: Makes formatting and style recommendations based on best practices and guidelines, which raises the level of uniformity and quality of the code
1. **Code search**: This feature allows the programmer to search for code based on natural language queries. The search results can be sorted and filtered based on relevance, popularity, or other criteria.

 To Automatic bug detection and patching
 1. **Coming soon**

#### Maturity Plan

This category is currently at minimal maturity. We will share further maturity plans as we receive feedback from our first beta experiences in the coming milestones.

Currently, we support the following languages with the highest confidence: Python, C, C++, Java, Javascript, and Go.

Extended programming language: Python, C, C++, Java, JavaScript, Go, C'#', Scala, Kotlin, PHP, Ruby, Rust, Typescript. 

### User success metrics

We plan to [measure the success of this category](https://gitlab.com/gitlab-org/gitlab/-/issues/375284) based on the following metrics:

* **Developer cycle time** - Code Suggestions may make developers more efficient and we believe help reduce the number of bugs, security vulnerabilities, and stylistic issues improving developer cycle time.
* **Time to merge** - With more efficient coding, merge requests will merge faster with less review time and back and forth between reviewers and authors.
* **Developer [SUS](/handbook/product/ux/performance-indicators/system-usability-scale/)** - Improved coding experience may directly improve the developer's usability and satisfaction with the GitLab development experience.


## Competitive Landscape

Please see the content in our [internal handbook](https://internal.gitlab.com/handbook/product/best-in-class/modelops)

## Analyst Landscape

As this category is new, we are actively engaging analysts on new reports and research, we'll share links to those as they are published.

