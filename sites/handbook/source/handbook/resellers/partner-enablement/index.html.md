---
layout: handbook-page-toc
title: "Channel Partner Technical Presales Enablement"
---


## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />
{::options parse_block_html="true" /}


In this section of the Channel Partner Programs Handbook we review assets and enablement from the perspective of Channel Partners' Technical Presales constituencies.

Beyond our Channel Partners Handbook pages, you will find sales guides, use cases, training materials, and program guides reviewed below and hosted and updated in our [GitLab Partner Portal](https://partners.gitlab.com/). The materials should be a great place to start effectively selling, serving and hitting your number with GitLab.

Rember that some of the links below require you to login to [GitLab's Partner Portal](https://partners.gitlab.com/) first. If you haven't already, [here is where you register for portal access](https://partners.gitlab.com/English/).

# Basic Presales Knowledge
{: .gitlab-orange}

## Get to know GitLab, what it is, what it offers, and what it does

1.  GitLab [promo video](https://player.vimeo.com/video/702922416) on [https://about.gitlab.com](https://about.gitlab.com) (1 min)

2.  GitLab [promo demo video](https://about.gitlab.com/demo/) (3 min)

3.  Get to know the general capability set at [https://about.gitlab.com/platform](https://about.gitlab.com/platform) (be sure to scroll down to the GitLab Features section and then click on the stages of the DevSecOps process (Plan, Create, Verify, Package, etc.) for more information

4.  Our [Solutions page](https://about.gitlab.com/solutions/) offers a wide variety of ways customers leverage GitLab


## Pitch GitLab and uncover new opportunities

1.  Please register for the [Building Pipelines GitLab Partner Webinar Series,](https://content.gitlab.com/viewer/63bddf02edadd0b1346a73db) where we discuss various sales and presales-level topics that help you build your sales pipeline with GitLab.

2.  Review [Positioning GitLab - Handbook Page](https://about.gitlab.com/handbook/positioning-faq/)

3.  Discovery is a crucial skill for any Solutions Architect (SA).  [Here is how GitLab SA’s do their Discovery](https://about.gitlab.com/handbook/sales/qualification-questions/#questions-for-selling-gitlab-premium-and-ultimate).

4.  Our [Customer Case Studies](https://about.gitlab.com/customers/) will give you ideas of why enterprises are choosing GitLab.  Bookmark this for customer justification in deals later.

5.  There are different [User Personas](https://about.gitlab.com/handbook/product/personas/) vs. [Buyer Personas](https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/roles-personas/buyer-persona/) to understand when selling a Platform like GitLab.  Which do you value more?


## Learn how to solution and transact a GitLab deal

1.  GitLab has a leading market share in self-managed deployments.  Here are our [reference architectures](https://docs.gitlab.com/ee/administration/reference_architectures/) and [install guides](https://docs.gitlab.com/ee/install/).  

2.  SaaS on GitLab.com is an increasingly popular option.  [Start a Free Trial](https://gitlab.com/-/trials/new) now.

3.  Pricing is a simple per-user subscription that is the same whether you are using SaaS or Self Hosted instance.  Customers see the [List Price here](https://about.gitlab.com/pricing/).  Review the [Partner Program Incentives Guide](https://partners.gitlab.com/prm/English/s/assets?id=350001) for more details on partner transactions.

4.  There are [online ROI calculators;](https://about.gitlab.com/calculator/), your partner team can share more advanced ROI tools.


## Services opportunities when selling Gitlab

1.  Here are some [service kits](https://partners.gitlab.com/prm/English/c/Channel_Service_Packages) we have developed, including sample Statements of Work (SOWs) and Levl of Effort (LOEs).

2.  These are the GitLab PS [Delivery Kits](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits) our teams use for customer projects.


# Advanced Presales & Technical Knowledge
{: .gitlab-orange}

## Compete

* [GitLab Maturity Page](https://about.gitlab.com/direction/maturity/): A very transparent assessment of the maturity of each GitLab feature. No direct comparisons with other vendors here.

* [GitLab Customer Success Stories](https://about.gitlab.com/customers/): Our success stories, including customer tesimonials.

## Technical

* [GitLab Documentation](https://docs.gitlab.com/): The best starting point for technical questions. It lists feature descriptions, excellent tutorials, and troubleshooting tips.

* [GitLab Blog](https://about.gitlab.com/blog/): Various content about GitLab. Mostly technical stuff, but not exclusively. Subscribing to the newsletter can be useful.

* [Release Overview Website](https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/what-is-new-since/?tab=features): A fairly unknown tool that can help with understanding the changes in each GitLab version. (Source code of the website is [here](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/what-is-new-since).) Important features of this website to pay extra attention to:

    * [CVEs by version](https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/what-is-new-since/?tab=cves): easily keep track of security vulnerabilities in each GitLab version.

    * [Upgrade path](https://gitlab-com.gitlab.io/support/toolbox/upgrade-path/): a handy tool which can list the steps of upgrading self-managed GitLab from version X to version Y.

* [Deprecations by version](https://docs.gitlab.com/ee/update/deprecations.html): same data as [this page](https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/what-is-new-since/?tab=deprecations) of the Release Overview website, just in a different format.

* [GitLab Releases Blog](https://about.gitlab.com/releases/categories/releases/): The same information as the Release Overview website, but in nicely formatted blog posts, ready to share with customers.

* [GitLab's Repository](https://gitlab.com/gitlab-org/gitlab): The source code of the GitLab software itself. The most useful part is the [Issues page](https://gitlab.com/gitlab-org/gitlab/-/issues), where everybody can check the state of a feature or a bug and see if it's planned or being worked on.


# Running Effective GitLab Demos and POCs
{: .gitlab-orange}

## Demo resources to get started showcasing the Platform

1.  The GitLab [Demo Handbook Page.](https://about.gitlab.com/handbook/customer-success/solutions-architects/demonstrations/)

2.  Check out the presentation and video in [Building Pipelines Partner Webinar](https://content.gitlab.com/viewer/63bddf02edadd0b1346a73db) Episode 011 on “Effective GitLab Demos.” (40:16)  More resources and helpful hints are provided.

3.  [GitLab Product Marketing](https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/#key-demo-videos) has some product demo videos on their handbook page.

4. [GitLab Partner Demo Delivery Guide](https://gitlab.com/gitlab-partner-demos/delivery-guide): a repository containing all the resources for partners to successfully deliver a 1-hour-long, high-level, technical overview demo of GitLab.


## Existing demo projects and guides you can leverage

1.  [Ultimate GitOps Workshop](https://guided-explorations.gitlab.io/workshops/gitlab-for-eks/): Leverage GitLab K8s agent to deploy to EKS.

1.  GitLab initiative to create demos you can leverage: [GitLab Learn Labs.](https://gitlab.com/gitlab-learn-labs/webinars/how-to-use-these-projects)

1.  Sometimes the demo script is [found in an issue](https://gitlab.com/gitlab-learn-labs/webinars/workshop-project/-/issues/1), not the Readme.  Not every demo project has a script (sorry).

1.  [Guided Explorations](https://gitlab.com/guided-explorations):  Contains Joint Reference Architectures, Patterns, and Working Examples for integrating GitLab with Alliance and Technology solutions.

1.  [Customer Success Workshops](https://gitlab.com/gitlab-com/customer-success/workshops/templates):  A working group containing all CS Workshop Templates.

1.  [SA Demo Catalog](https://gitlab.com/gitlab-com/customer-success/solutions-architecture/demo-catalog):  Demo Archive of SA Demo's which can be reused or repurposed for your demos

1. [CI Samples Demo](https://gitlab.com/gitlab-learn-labs/webinars/cicd/cicd-samples):  CI functions samples for quick demos

## Proof of Value (POV), aka POC’s


Here’s the [handbook page](https://about.gitlab.com/handbook/customer-success/solutions-architects/tools-and-resources/pov/) for conducting POVs


# Additional Resources
{: .gitlab-orange}

## Third-Party Tutorials

1.  **GitLab for Beginners:**
    - [Learn GitLab in 3 Hours GitLab Complete Tutorial For Beginners](https://www.youtube.com/watch?v=8aV5AxJrHDg) (~ 3 hrs)
1.  **GitLab Flow:**
    - [GitLab Flow - GitLab Tutorial - Part I](https://www.youtube.com/watch?v=ZJuUz5jWb44) (15:37)
    - [GitLab Flow - GitLab Tutorial - Part II](https://www.youtube.com/watch?v=K5Ux_m0ccuo&list=RDCMUCxu8xrbPZ-neg3FhMdz4yrA&index=3) (33:00)
1.  **GitLab CI/CD:**
    - [GitLab CI/CD - GitLab Runner Introduction - 2022](https://www.youtube.com/watch?v=-CyVpfDQAG0&list=RDCMUCxu8xrbPZ-neg3FhMdz4yrA&index=6)(23:27)
    - [GitLab CI CD - Install and Configure GitLab Runner on Kubernetes with Helm](https://www.youtube.com/watch?v=0Fes86qtBSc) (29:41)
1.  **GitLab Unfiltered:**
    - GitLab Security Capabilities
[A tour of GitLab Security capabilities](https://www.youtube.com/watch?v=d8X6vYY4WOA) (50:16)
1. **Other Good Assets:**
    - [Partner Enablement Presentation](https://docs.google.com/presentation/d/1VUY6WR4gewnXITUUAnVbFOExXmhdTYif2JgAiTbT7tE/edit#slide=id.g12b319f6181_0_10)
