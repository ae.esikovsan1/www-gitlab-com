---
layout: markdown_page
title: "Working Groups"
description: "Like all groups at GitLab, a working group is an arrangement of people from different functions. Learn more!"
canonical_path: "/company/team/structure/working-groups/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What's a Working Group?

Like all groups at GitLab, a [working group](https://en.wikipedia.org/wiki/Working_group) is an arrangement of people from different functions. What makes a working group unique is that it has defined roles and responsibilities, and is tasked with achieving a high-impact business goal fast. A working group disbands when the goal is achieved (defined by exit criteria) so that GitLab doesn't accrue bureaucracy.

Working groups are for important work that needs to be done quickly, when asynchronous work would be too slow.

### CEO Handbook Learning Discussion on Working Groups

GitLab's CEO, Sid, and Chief of Staff to the CEO, Stella, and the Learning & Development team discuss Working Groups in detail during a [CEO handbook learning session](/handbook/people-group/learning-and-development/learning-initiatives/#ceo-handbook-learning-sessions).

Topics covered include:

1. What is a working group
1. When to start a working group
1. Difference between project managers and working groups.
   - Note: GitLab does not internally have project managers. Individual team members should have agency and accountability. We don't want someone who just tracks the status of work and makes updates. We want people to do this directly, and we want the [DRI](/handbook/people-group/directly-responsible-individuals/) to own the process and outcome. This supports accountability. We sometimes have project managers when interacting with external organizations, because accountability is harder when working with external parties.
1. Lessons learned from previous working groups
1. Why and how working groups support cross-functional projects

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/tE3d8WUSL30" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Roles and Responsibilities

### Required Roles

#### Working Group Directly Responsible Individual

This is the **person ultimately responsible for the success of the Working Group**, and also plays the facilitator role. A facilitator is responsible for running meetings and supporting the operational efficiency and success of a working group.

Please see the [process](#process) below for details on the responsibilities of this role.

#### Executive Sponsor

The E-Group member who is responsible for staying plugged into the project, supporting the Working Group DRI (if necessary), and supporting escalations (if required). This is the E-Group member most interested in the results or responsible for the outcome. It will usually be the E-Group Member who owns the function that the Working Group DRI belongs to. E-Group Sponsors should be kept in the loop--especially when there are changes to timeline/scope, interdependencies that require alignment, or input needed. They will usually attend Working Group Meetings.

The E-Group Sponsor is responsible for helping the Working Group DRI to obtain funding needed for Working Group success. This is important as the E-Group is the level at which budget dollars are owned.

The E-Group sponsor also plays a key role in being a bridge to the rest of E-Group. This person should help to flag when related topics or updates should be brought to E-Group as an update or ask.

#### Functional Lead

One or more people who represent their entire function to the working group, regularly monitors the Working Group Slack Channel, creates issues for action items, serves as DRI for issues created, actively participates in meetings, volunteers for opportunities to further the Working Groups goals, regularly attends meetings either synchronously or asynchronously, shares information learned from the Working Group with their Functional teams, volunteers to take on action items , gathers feedback from Functional teams and brings that feedback back to the Working Group.

### Optional Roles

#### Member

Any subject matter expert, attends meetings synchronously or asynchronously on a regular basis, regularly monitors the Working Group Slack Channel, shares information learned from the Working Group with their peers, and gathers feedback from their peers and brings that feedback back to the Working Group. A member may serve as a DRI for a specific workstream or activity.

## Guidelines

1. An executive sponsor is required, in part, to prevent proliferation of working groups.
1. A person should not facilitate more than one concurrent working group.
1. Participation in some Working Groups requires a significant time commitment. Participants should be clear on their role and expectations for what they are expected to deliver. They should manage their time and capacity and quickly escalate if they feel unable to serve in or deliver in their role.
1. It is highly recommended that anyone in the working group with [OKRs](/company/okrs/) aligns them to the effort.

## Process

### Map out the Working Group

The first step is to carefully map out what type of activities are required for the Working Groups success. The DRI can get recommendations on who to involve, but they should first have a view of the jobs to be done.

### Creating the Working Group

The DRI should identify key folks who will be members of the Working Group. They could do this in a few ways:

1. Reach out to the exec sponsor to help identify leads/contacts throughout the organization who could provide support
1. Reach out to functional leads from the stages identified as needing to provide support to this effort
1. Include the folks who the DRI has already been working with on this effort, and ask them if they'd like to continue or have someone they'd recommend taking over
1. Solicit help on the respective stage's/team's Slack channel. The [Features by Group](/handbook/product/categories/features/) page may provide some guidance

For example, if you know that this Working Group will eventually involve customer communications, you should ensure that the team has appropriate representation from the Customer Success department and that the representative is clear on the asks for both the team and the individual within the coming weeks and quarters.

### Establishing team norms for the Working Group

The DRI should establish how the Working Group will work, including:

1. Frequency of meetings
1. Inclusivity of team members across the globe
1. Frequency of status updates for use in status tracker
1. Designating DRI for overall project management, including scheduling meetings and tracking project status
1. Create a Slack channel for collaboration. The Slack channel name should start with the `#wg_` prefix.
1. Gather metrics that will tell you when the goal is met
1. Organize activities that should provide incremental progress
1. Ship iterations and track the metrics
1. Communicate the results

The Working Group should follow the [GitLab norms for meetings](/company/culture/all-remote/meetings/) including:

1. Have an agenda
1. Document everything
1. Save time at the end of the meeting to take action items
1. Turn action items into merge requests or issues

Working Groups should follow the guidance outlined in [Building High Performing Teams](/handbook/leadership/build-high-performing-teams/).

### Working Group meetings

The DRI should stand up meetings at the cadence appropriate for the Working Group, depending on the urgency/importance and timelines associated. The cadence of meetings should map to how quickly you are moving and the amount of synchronously touch points needed for alignment and decision making input.

For example, you may have a high priority project in which decisions have been signed off on and change is not anticipated. Folks are clear on their roles and are staying on top of their activities as tracked in issues and epics. You may not need to meet more than once every two weeks to ensure alignment. Alternatively, you may be part of a project in which new deliverables are being reviewed daily and fast decisions have to be made. In this instance, it is appropriate to meet more than once a week.

All meetings should have an [agenda](/company/culture/all-remote/meetings/#4-all-meetings-must-have-an-agenda). [Live Doc Meetings](/company/culture/all-remote/live-doc-meetings/) have Google Docs as the preferred tool for taking meeting notes in an agenda. Please use the [GitLab Live Doc Meeting Agenda Template](https://docs.google.com/document/d/1eH-adpjfyo_RnlfbPvJ3i0e1Qb-aVoNc4yajnkZgJcU) as a starting point. If there's no agenda for an upcoming session, cancel the meeting.

### Create a page in the handbook

Working Groups should have a page in the handbook in the current `working-groups` folder and added to the [list of active working groups](#active-working-groups-alphabetic-order) below. If you inherited an existing Working Group, make sure that the page is up-to-date. This ensures that other team members have a place to go if they are looking to learn about what you are working on. At GitLab, we’re [public handbook first](/handbook/handbook-usage/#why-handbook-first), but if your work is [not public](/handbook/communication/confidentiality-levels/#internal), please use the internal handbook to keep the content [SAFE](/handbook/legal/safe-framework/).

Items to cover on your handbook page should include:

1. Name of initiative
1. Overview: what is this and why are we doing it?
1. Desired business outcomes, including exit criteria
1. Clearly outline risks and interdependencies. Flag mitigations and ensure that interdependencies are known and being addressed as part of the plan.
1. Key milestones and their delivery timeline
1. Who is involved. This should include who the DRI and Exec Sponsor (if there is one) are. Ensure that responsibilities for all participants are clearly documented and understood.

When possible, we work [handbook first](/handbook/handbook-usage/#why-handbook-first) and start with a [Merge Request](/handbook/communication/#start-with-a-merge-request). Google Docs can be used for drafting a proposal in which multiple folks need to collaborate in real-time. It may also be something that you'd consider when material is [limited access](/handbook/communication/confidentiality-levels/#limited-access). Once the confidentiality concerns are addressed and real-time editing is less crucial, the content should be moved to a [Merge Request](/handbook/communication/#start-with-a-merge-request) and the Google Doc should be marked deprecated with a link to the Merge Request.

### Create issues and epics

Use [issues](https://docs.gitlab.com/ee/user/project/issues/) to lay out the work defined, including the work defined in the [handbook](#create-a-page-in-the-handbook) page. Use [labels](https://docs.gitlab.com/ee/user/project/labels.html) to denote progress (blocked, in progress, not started) and also highlight phases of work. The issues should be clear on what needs to be done, who the DRIs are from impacted teams, and the outcome expected. This applies to already ongoing work and work which is being newly scoped. If the team decides through an issue to make changes or implement new ways of doing things, document the change in the handbook.

You may also want to use an [issue board](https://docs.gitlab.com/ee/user/project/issue_board.html) as an overview of progress.

As work is being fleshed out and sub-projects are identified, some issues should be promoted to [epics](https://docs.gitlab.com/ee/user/group/epics/) to group other issues that are part of the same sub-project. It’s best practice to have an issue or sub-epic for each sub-project associated with the Working Group. Group sub-epics under one parent epic to track progress over time. As items are completed, close out the issues and epics and document progress in the handbook.

Like the handbook, issues and epics should be public by default. If an issue or epic contains material that needs to remain internal, they should be made [confidential](https://docs.gitlab.com/ee/user/project/issues/confidential_issues.html) or be in a [project](https://docs.gitlab.com/ee/user/project/) which is [private](https://docs.gitlab.com/ee/user/public_access.html#private-projects-and-groups). If an issue or epic can remain public, but a comment needs to be added that is internal only, you can use [internal notes](https://docs.gitlab.com/ee/user/discussions/#add-an-internal-note) to allow the issue to remain public while having a confidential conversation.

### Communicating status, updates, and changes

Communicate outcomes using [multimodal communication](/handbook/communication/#multimodal-communication). For example, you can use a primary epic to communicate current status and updates. The description of the epic should be kept up to date with the latest status, and a running log of updates should be left as comments to the epic. In the comment tag the respective E-Group sponsor (if applicable) as well as the relevant DRIs with `@` mentions. Additionally, ping the link to the current status comment to the Slack channel created in [establishing team norms](#establishing-team-norms) `@` mentioning the relevant folks.

Any material changes to the direction or plan for the Working Group should be put into the handbook page created, but general status and updates can be kept in the epic.

Changes to the timeline or schedule can have rippling downstream impacts. It is important to communicate these changes as early as possible to all relevant stakeholders. Shifting dates have the potential to negatively impact team members who may be tasked with communicating/guiding users/customers through these changes.

### Trust Building

When a Working Group forms, it is important to [build trust](/handbook/leadership/building-trust/) amongst the team members. This will help the Working Group function better, build credibility, and allow team members to be vulnerable with each other, if needed. Just as it’s important to do the work itself, it’s also important to take time for coffee-chats, activities, games, etc. that allow Working Group members to build a personal connection.

### Disband the working group

Once the Working Group has served it's intended purpose, it's time to disband the Working Group.

1. Celebrate. Being able to close a working group is a thing to be celebrated!
1. Move the working group to the [Past Working Groups](#past-working-groups-alphabetic-order) section on this page.
1. Update the working group's page with the close date and any relevant artifacts for prosperity.
1. Archive the slack channel.
1. Delete the recurring calendar meeting.

### Modifications to Process for Limited Access Communications

We make things public by default because [transparency is one of our values](/handbook/values/#transparency).
Some things can't be made public and are either [internal](#internal) to the company or have [limited access](#limited-access) even within the company.
If something isn't on our [Not Public list](/handbook/communication/confidentiality-levels/#not-public), we should make it available externally. If a working group is working on something on the Not Public List, working group team members should take precautions to limit access to information until it is determined that information can be shared more broadly. To highlight a few modifications to the process above:

1. Preparation
   1. Determine an appropriate project name using [limited access naming conventions](/handbook/communication/confidentiality-levels/#limited-access).
   1. Create an overview page and add the link to [Active Working Groups](/company/team/structure/working-groups/#active-working-groups-alphabetic-order). You can share limited information, but capture key team members, including the facilitator, executive stakeholder, and functional lead.
   1. If working in the handbook, evaluate whether the page should be confidential or be housed in a new project with limited access. Consider working in the [staging handbook](/handbook/handbook-usage/#the-staging-handbook). We use this when information may need to be iterated on or MR branches may need to be created in staging before it is made public. Outside of E-Group, temporary access may be granted on a project-specific basis.
   1. Maintain a list of working group members and other folks who are participating in or informed of the project. This list should be available to all participating team members. Folks should not be added to this list until it is confirmed that they understand what can be communicated.
   1. Ensure that each working group team member understands what can be communicated externally and internally.
   1. Have private Slack channels that include folks who are directly working on the project.
   1. Limit the agenda to a specific set of folks.
1. Communicate the results
   - Communicate results and progress to the direct working group or other key stakeholders to ensure that folks are aligned and have context on key happenings. Do not share sensitive information outside of private channels.
1. Proactively share information if the project is no longer limited access
   1. Notify widely of progress or exit outcomes when information can be shared more broadly.
   1. Evaluate which artifacts and communication material can be made internally available or public.
      1. If you were working in the [staging handbook](/handbook/handbook-usage/#the-staging-handbook), follow instructions to make a merge request against the public repo.
      1. Transition members to public Slack channels and archive private channels.
      1. Deprecate private agendas. Link this to a new agenda document.
      1. Consider making GitLab Groups and Projects public or avialable to a broader audience.

## Participating in a Working Group

If you are interested in participating in an active working group, it is generally recommended that you first communicate with your manager and the facilitator and/or lead of the working group. After that, you can add yourself to the working group member list by creating a MR against the specific working group handbook page.

If you are unable to attend the existing working group meeting due to time differences, you may approach the facilitator to arrange an alternative meeting.

## Active Working Groups (alphabetic order)

1. [Account Escalation Process](/company/team/structure/working-groups/account-escalation-process)
1. [AI Integration](/company/team/structure/working-groups/ai-integration/)
1. [API Vision](/company/team/structure/working-groups/api-vision/)
1. [Automotive Development](/company/team/structure/working-groups/automotive-development/)
1. [Category Leadership](/company/team/structure/working-groups/category-leadership/)
1. [CI/CD Build Speed time-to-result](/company/team/structure/working-groups/ci-build-speed/)
1. [ClickHouse Datastore](/company/team/structure/working-groups/clickhouse-datastore/)
1. [Customer Use Case Adoption](/company/team/structure/working-groups/customer-use-case-adoption/)
1. [Demo & Test Data](/company/team/structure/working-groups/demo-test-data/)
1. [Emerging Talent](/company/team/structure/working-groups/emerging-talent/)
1. [Event Stream](/company/team/structure/working-groups/event-stream/)
1. [Expense Management](/company/team/structure/working-groups/expense-management/)
1. [FedRAMP Execution](/company/team/structure/working-groups/fedramp-execution/)
1. [Frontend Observability](/company/team/structure/working-groups/frontend-observability/)
1. [Frontend Vision](/company/team/structure/working-groups/frontend-vision/)
1. [GCP Partnership](/company/team/structure/working-groups/gcp-partnership/)
1. [GitLab Dedicated](/company/team/structure/working-groups/gitlab-dedicated/)
1. [GitLab.com Disaster Recovery](/company/team/structure/working-groups/disaster-recovery/)
1. [GitLab.com SAAS Data Pipeline](/company/team/structure/working-groups/gitlab-com-saas-data-pipeline)
1. [Revenue Globalization](/company/team/structure/working-groups/globalization/)
1. [Issue Prioritization Framework](/company/team/structure/working-groups/issue-prioritization-framework/)
1. [Leading Organizations](/company/team/structure/working-groups/leading-organizations/)
1. [Learning Experience](/company/team/structure/working-groups/learning-experience/)
1. [Modern Applications Go-To-Market](/company/team/structure/working-groups/modern-applications-gtm/)
1. [Next Architecture Workflow](/company/team/structure/working-groups/next-architecture-workflow/)
1. [Product Accessibility](/company/team/structure/working-groups/product-accessibility/)
1. [Runtime Update Process](/company/team/structure/working-groups/runtime-update-process)
1. [Software Supply Chain Security](/company/team/structure/working-groups/software-supply-chain-security/)
1. [Talent Acquisition SSOT](/company/team/structure/working-groups/recruiting-ssot/)
1. [Token Management](/company/team/structure/working-groups/token-management/)
1. [Vue.js 3 Upgrade](/company/team/structure/working-groups/vuejs-3-migration/)


## Past Working Groups (alphabetic order)

1. [Architecture Kickoff](/company/team/structure/working-groups/architecture-kickoff/)
1. [China Service](/company/team/structure/working-groups/china-service/)
1. [CI Queue Time Stabilization](/company/team/structure/working-groups/ci-queue-stability/)
1. [Commercial & Licensing](/company/team/structure/working-groups/commercial-licensing/)
1. [Continuous Scanning](/company/team/structure/working-groups/continuous-scanning/)
1. [Cross Functional Prioritization](/company/team/structure/working-groups/cross-functional-prioritization/)
1. [Contributor Growth](/company/team/structure/working-groups/contributor-growth)
1. [Dashboards](/company/team/structure/working-groups/dashboards/)
1. [Database Scalability](/company/team/structure/working-groups/database-scalability/)
1. [Development Metrics](/company/team/structure/working-groups/development-metrics/)
1. [Dogfood Plan](/company/team/structure/working-groups/dogfood-plan/)
1. [Ecommerce Motion](/company/team/structure/working-groups/ecommerce)
1. [Engineering Career Matrices](/company/team/structure/working-groups/engineering-career-matrices/)
1. [Engineering Internship](/company/team/structure/working-groups/engineering-internship/)
1. [Enterprise Market Leadership](/company/team/structure/working-groups/enterprise-market-leadership)
1. [Experimentation](/company/team/structure/working-groups/experimentation/)
1. [Githost Migration](/company/team/structure/working-groups/githost-migration/)
1. [GitLab Administration](/company/team/structure/working-groups/administration/)
1. [GitLab.com Cost](/company/team/structure/working-groups/gitlab-com-cost/)
1. [GitLab.com Revenue](/company/team/structure/working-groups/gitlab-com-revenue/)
1. [gitlab-ui (CSS and Components)](/company/team/structure/working-groups/gitlab-ui/)
1. [GTM Product Analytics](/company/team/structure/working-groups/product-analytics-gtm/)
1. [IACV - Delta ARR](/company/team/structure/working-groups/iacv-delta-arr/)
1. [IC Gearing](/company/team/structure/working-groups/ic-gearing/)
1. [Improve Ops Quality](/company/team/structure/working-groups/improve-ops-quality/)
1. [Internship Pilot](/company/team/structure/working-groups/internship-pilot/)
1. [Internal Feature Flag usage](/company/team/structure/working-groups/feature-flag-usage/)
1. [Isolation](/company/team/structure/working-groups/isolation/)
1. [Learning Restructure](/company/team/structure/working-groups/learning-restructure)
1. [Licensing and Transactions Improvements](/company/team/structure/working-groups/licensing-transactions-improvements/)
1. [Log Aggregation](/company/team/structure/working-groups/log-aggregation/)
1. [Logging](/company/team/structure/working-groups/logging/)
1. [Maintainership](/company/team/structure/working-groups/maintainership/)
1. [Merge Request Report Widgets](/company/team/structure/working-groups/merge-request-report-widgets/)
1. [Minorities in Tech (MIT) Mentoring Program](/company/team/structure/working-groups/mit-mentoring/)
1. [MLOps](/company/team/structure/working-groups/mlops/)
1. [Multi-Large](/company/team/structure/working-groups/multi-large/)
1. [Object Storage](/company/team/structure/working-groups/object-storage/)
1. [Pipeline Validation Service Operations](/company/team/structure/working-groups/pipeline-validation-service-operations/)
1. [Performance Indicators](/company/team/structure/working-groups/performance-indicators/)
1. [Product Analytics](/company/team/structure/working-groups/product-analytics/)
1. [Product Career Development Framework](/company/team/structure/working-groups/product-career-development-framework/)
1. [Product Development Flow](/company/team/structure/working-groups/product-development-flow/)
1. [Product Engagement Actions (FY21)](/company/team/structure/working-groups/FY21-product-engagement-actions/)
1. [Purchasing Reliability](/company/team/structure/working-groups/purchasing-reliability/)
1. [Rate Limit Architecture](/company/team/structure/working-groups/rate-limit-architecture/)
1. [Real-Time](/company/team/structure/working-groups/real-time/)
1. [Secure Offline Environment Working Group](/company/team/structure/working-groups/secure-offline-environment/)
1. [Self-Managed Scalability](/company/team/structure/working-groups/self-managed-scalability/)
1. [Sharding](/company/team/structure/working-groups/sharding/)
1. [Simplify Groups & Projects](/company/team/structure/working-groups/simplify-groups-and-projects/)
1. [Single Codebase](/company/team/structure/working-groups/single-codebase/)
1. [SOX PMO](/company/team/structure/working-groups/sox/)
1. [TeamOps Sales and Marketing Group](/company/team/structure/working-groups/teamops-sales-marketing/)
1. [Tiering](/company/team/structure/working-groups/tiering/)
1. [Transient Bugs](/company/team/structure/working-groups/transient-bugs/)
1. [Upstream Diversity](/company/team/structure/working-groups/upstream-diversity/)
1. [Webpack (Frontend build tooling)](/company/team/structure/working-groups/webpack)

## Top Cross-Functional Initiatives

Please see the handbook page on [Top Cross-Functional Initiatives](/company/top-cross-functional-initiatives/)
